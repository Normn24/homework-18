"use strict";

const passwordEnter = document.querySelector("#passwordEnter");
const passwordRepit = document.querySelector("#passwordRepit");
const eyeIconOne = document.querySelector("#eye1");
const eyeIconTwo = document.querySelector("#eye2");
const errorMessage = document.querySelector("#error-message");

function togglePasswordVisibility(input, icon) {
  icon.addEventListener("click", () => {
    icon.classList.toggle("fa-eye-slash");
    const type = input.getAttribute("type") === "password" ? "text" : "password";
    input.setAttribute("type", type);

  })
}

togglePasswordVisibility(passwordEnter, eyeIconOne);
togglePasswordVisibility(passwordRepit, eyeIconTwo);

const submitBtn = document.querySelector(".password-form")

submitBtn.addEventListener("submit", function (event) {
  event.preventDefault();

  const pass1 = passwordEnter.value;
  const pass2 = passwordRepit.value;

  if (pass1 === pass2 && pass1 && pass2) {
    alert("You are welcome");
    errorMessage.innerText = " ";
  } else {
    errorMessage.innerText = "The passwords don't match!!!";
  }
});